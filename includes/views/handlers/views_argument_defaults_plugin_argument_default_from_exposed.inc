<?php

/**
 * Default argument plugin to get the current user's cart order ID.
 */
class views_argument_defaults_plugin_argument_default_from_exposed extends views_plugin_argument_default {

  /**
   * @inheritdoc
   */
  function get_argument() {
    if (!empty($this->options['exposed_filter']) && !empty($_GET[$this->options['exposed_filter']])) {
      return check_plain($_GET[$this->options['exposed_filter']]);
    }
    return 0;
  }

  /**
   * @inheritdoc
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['exposed_filter'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  /**
   * @inheritdoc
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();
    $filters = $this->view->display_handler->get_option('filters');
    foreach ($filters as $filter) {
      if (!empty($filter['expose']['identifier'])) {
        $options[$filter['expose']['identifier']] = $filter['expose']['identifier'];
      }
    }
    if ($options) {
      $form['exposed_filter'] = array(
        '#type' => 'select',
        '#required' => TRUE,
        '#title' => t('Exposed filter'),
        '#options' => $options,
        '#default_value' => $this->options['exposed_filter'],
        '#description' => t('The filter identifier of the exposed filter to get value from'),
      );
    }
    else {
      $form['exposed_filter'] = array(
        '#markup' => t('Add an exposed filter to this view to be able to use this option as a default argument'),
      );
    }
  }

}
