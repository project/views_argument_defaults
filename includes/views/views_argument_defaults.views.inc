<?php

/**
 * @file
 * The views_argument_defaults.views.inc file.
 */

/**
 * Implements hook_views_plugins().
 */
function views_argument_defaults_views_plugins() {
  $plugins = array(
    'argument default' => array(
      'views_argument_defaults_plugin_argument_default_from_exposed' => array(
        'title' => t("Get default from exposed filter"),
        'handler' => 'views_argument_defaults_plugin_argument_default_from_exposed',
        'path' => drupal_get_path('module', 'views_argument_defaults') . '/includes/views/handlers',
      ),
    ),
  );
  return $plugins;
}
